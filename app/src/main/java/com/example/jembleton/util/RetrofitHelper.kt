package com.example.jembleton.util

import com.example.jembleton.API.StatusAPI
import com.example.jembleton.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofiHelper {



    val getClient: StatusAPI
        get() {

            val gson = GsonBuilder()
                .setLenient()
                .create()
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(logger).build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.API_Get_Status)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            return retrofit.create(StatusAPI::class.java)
        }
}