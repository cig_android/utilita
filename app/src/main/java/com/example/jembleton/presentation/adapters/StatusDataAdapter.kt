package com.example.jembleton.presentation.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.jembleton.R
import com.example.jembleton.model.StatusModel
import com.example.jembleton.presentation.activities.ActivityStatus

class StatusDataAdapter(
    private var statusList: List<StatusModel>,
    private val context: Context,
    private val onClickListener: (View, StatusModel) -> Unit
) : RecyclerView.Adapter<StatusDataAdapter.ViewHolder>() {

    var itemPos: Int = 0

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.status_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        itemPos = position
        val statusModel = statusList[position]

        val header = statusModel.name
        val uri = statusModel.uri
        val responseCode = statusModel.responseCode
        val responseTime = statusModel.responseTime
        val type = statusModel.type

        if (uri.equals(null)) {
            holder.statusTextViewURIHeader.isVisible = false
            holder.statusTextViewURI.isVisible = false
        } else {
            holder.statusTextViewURIHeader.isVisible = true
            holder.statusTextViewURI.isVisible = true
        }

        holder.statusTextViewHeader.text = header
        holder.statusTextViewURI.text = uri
        holder.statusTextViewResponseCode.text = responseCode.toString()
        holder.statusTextViewResponseTime.text = responseTime.toString()
        holder.statusTextViewClassType.text = type
        holder.itemView.setOnClickListener { view ->
            onClickListener.invoke(view, statusModel)
            val statusIntent = Intent(context, ActivityStatus::class.java)
            statusIntent.putExtra(context.resources.getString(R.string.name), header)
            statusIntent.putExtra(context.resources.getString(R.string.uriString), uri)
            statusIntent.putExtra(context.resources.getString(R.string.responseCode), responseCode)
            statusIntent.putExtra(context.resources.getString(R.string.responseTime), responseTime)
            statusIntent.putExtra(context.resources.getString(R.string.type), type)
            context.startActivity(statusIntent)
        }
    }

    override fun getItemCount(): Int {
        return statusList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var statusTextViewHeader: TextView = itemView.findViewById(R.id.tv_header)
        var statusTextViewURI: TextView = itemView.findViewById(R.id.tv_uri)
        var statusTextViewURIHeader: TextView = itemView.findViewById(R.id.tv_uriHeader)
        var statusTextViewResponseCode: TextView = itemView.findViewById(R.id.tv_responseCode)
        var statusTextViewResponseTime: TextView = itemView.findViewById(R.id.tv_responseTime)
        var statusTextViewClassType: TextView = itemView.findViewById(R.id.tv_classType)

    }


}