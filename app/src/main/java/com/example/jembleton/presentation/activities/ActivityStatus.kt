package com.example.jembleton.presentation.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.jembleton.R

class ActivityStatus : AppCompatActivity() {

    private lateinit var header: String
    private lateinit var uriTextHeader: TextView
    private lateinit var uri: String
    private lateinit var responseCode: String
    private lateinit var responseTime: String
    private lateinit var type: String
    private lateinit var headerTextView: TextView
    private lateinit var uriTextView: TextView
    private lateinit var responseCodeTextView: TextView
    private lateinit var resposneTimeTextView: TextView
    private lateinit var typeTextView: TextView
    private lateinit var closeButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status)

        val intent = intent
        val data = intent.extras

        getData(intent, data)
        initWidgets()
        setListeners()
        setTextViewData()
    }

    private fun getData(pIntent: Intent, pData: Bundle?) {

        if (pIntent.hasExtra(resources.getString(R.string.name))) {
            header = pData?.get(resources.getString(R.string.name)) as String
        }

        if (pIntent.hasExtra(resources.getString(R.string.uriString))) {
            if (null !=  pData?.get(resources.getString(R.string.uriString))) {
                    uri = pData.get(resources.getString(R.string.uriString)) as String
            } else {
                uri = resources.getString(R.string.noData)
            }
        }

        if (pIntent.hasExtra(resources.getString(R.string.responseCode))) {
            responseCode = pData?.get(resources.getString(R.string.responseCode)).toString()
        }

        if (pIntent.hasExtra(resources.getString(R.string.responseTime))) {
            responseTime = pData?.get(resources.getString(R.string.responseTime)).toString()
        }
        if (pIntent.hasExtra((resources.getString(R.string.type)))) {
            type = pData?.get(resources.getString(R.string.type)) as String
        }
    }


    private fun initWidgets() {
        headerTextView = findViewById(R.id.tv_headerText)
        uriTextHeader = findViewById(R.id.tv_UrlHeader)
        uriTextView = findViewById(R.id.tv_UrlBody)
        responseCodeTextView = findViewById(R.id.tv_ResponseCodeBody)
        resposneTimeTextView = findViewById(R.id.tv_ResponseTimeBody)
        typeTextView = findViewById(R.id.tv_typeBody)
        closeButton = findViewById(R.id.btn_close)
    }

    private fun setListeners(){

        closeButton.setOnClickListener{
            finish()
        }
    }

    private fun setTextViewData() {
        headerTextView.text = header
        if(uri != resources.getString(R.string.noData)) {
            uriTextView.isVisible = true
            uriTextView.text = uri
        } else {
            uriTextHeader.isVisible = false
        }
        responseCodeTextView.text = responseCode
        resposneTimeTextView.text = responseTime
        typeTextView.text = type
    }
}