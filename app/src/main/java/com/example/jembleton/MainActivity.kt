package com.example.jembleton

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jembleton.model.StatusModel
import com.example.jembleton.presentation.adapters.StatusDataAdapter
import com.example.jembleton.tokens.Statuses
import com.example.jembleton.util.RetrofiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    lateinit var statusRecyclerView: RecyclerView
    lateinit var statusAdapter: StatusDataAdapter

    var dataList = ArrayList<StatusModel>()

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        statusRecyclerView = findViewById(R.id.rv_statuses)
        statusRecyclerView.adapter =
            StatusDataAdapter(dataList, this, onClickListener = { _, _ -> Unit })
        statusRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        getData()

    }

    private fun getData() {
        val call: Call<Map<String, Map<String, Statuses>>> = RetrofiHelper.getClient.getResponse()
        call.enqueue(object : Callback<Map<String, Map<String, Statuses>>> {

            override fun onResponse(
                call: Call<Map<String, Map<String, Statuses>>>,
                response: Response<Map<String, Map<String, Statuses>>>
            ) {
                Log.e(TAG, "Success calling client")


                val allStatuses = response.body()
                val listOfStatuses = HashMap<String, Statuses>()

                allStatuses?.forEach { (_, value) ->
                    value.forEach { (k, v) ->
                        listOfStatuses[k] = v
                    }
                }
                allStatuses?.forEach { (_, value) ->
                    value.forEach { (k, v) ->
                        dataList.add(
                            StatusModel(
                                k,
                                v.uri,
                                v.responseCode,
                                v.responseTime,
                                v.type
                            )
                        )
                    }
                }

                statusRecyclerView.adapter?.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<Map<String, Map<String, Statuses>>>?, t: Throwable?) {
                Log.e(TAG, "Error calling client: ${t.toString()}")
            }
        })
    }
}
