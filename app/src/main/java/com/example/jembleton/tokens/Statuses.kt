package com.example.jembleton.tokens

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Johnny on 25/01/2021
 */
data class Statuses(

//    var name: String,

    @Expose
    @SerializedName("url")
    val uri: String?,
    @Expose
    @SerializedName("responseCode")
    val responseCode: Int?,
    @Expose
    @SerializedName("responseTime")
    val responseTime: Double?,
    @Expose
    @SerializedName("class")
    val type: String?
        )
{}