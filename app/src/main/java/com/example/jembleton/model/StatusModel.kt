package com.example.jembleton.model

/**
 * Created by Johnny on 27/01/2021
 */
class StatusModel (pName: String?, pUri: String?, pResponseCode: Int?, pResponseTime: Double?, pType: String? ){

    val name = pName
    val uri = pUri
    val responseCode = pResponseCode
    val responseTime = pResponseTime
    val type = pType
}