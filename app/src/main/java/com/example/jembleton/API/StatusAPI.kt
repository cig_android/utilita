package com.example.jembleton.API

import com.example.jembleton.tokens.Statuses
import retrofit2.Call
import retrofit2.http.GET


/**
 * Created by Johnny on 25/01/2021
 */
interface StatusAPI {

    @GET("status")
    fun getResponse():  Call<Map<String, Map<String,Statuses>>>
}